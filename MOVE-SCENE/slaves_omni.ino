//PA9 PA8-pwm fata+spate
//PB15 PB14-enable 
//PB6 PB7-pwm fata+spate
//folosim serial2 pentru comunicatie cu master PA3 si PA2
int ind=0;
long vStg=63, vDr=63;
int urcat = LOW;
void setup() {
  //pinMode(PA6, PWM);
  pwmWrite(PA6,0); //max 65535 la 549.1Hz
  pinMode(PC13, OUTPUT);
  //##### pentru driver #####
 pinMode(PA9,PWM);
 pinMode(PA8,PWM);
 pinMode(PB15,OUTPUT); //ptr enable driver
 pinMode(PB14,OUTPUT); //ptr enable driver
 pwmWrite(PA9,0); //max 65535 la 549.1Hz
 pwmWrite(PA8,0); //max 65535 la 549.1Hz

 pinMode(PB6,PWM);
 pinMode(PB7,PWM);
 pinMode(PB8,OUTPUT); //ptr enable driver
 pinMode(PB9,OUTPUT); //ptr enable driver
 pwmWrite(PB6,0); //max 65535 la 549.1Hz
 pwmWrite(PB7,0); //max 65535 la 549.1Hz
 

  //activam enable-urile
  digitalWrite(PB15, HIGH);
  digitalWrite(PB14, HIGH);
  digitalWrite(PB8, HIGH);
  digitalWrite(PB9, HIGH);
 //####pentru comunicatia cu masterul, 
 Serial2.begin(57600);
 digitalWrite(PC13, HIGH);
}

void loop() 
{
  byte aux=0;
  byte date_noi=0;
  byte datele2=0, datele3=0;
  long vStg_tmp = 0, vDr_tmp = 0;
  digitalWrite(PC13, LOW);
  if (Serial2.available() > 0) 
  {
  // read the incoming byte:
    date_noi = Serial2.read();
    if(date_noi == 0b10000011)
    {
      urcat = HIGH;
    } else if(date_noi == 0b10000000) {
      urcat = LOW;
    } else 
    {
         aux=(date_noi & 0b01000000) << 1; 
         aux=((date_noi & 0b00100000) << 2)^aux; 
         aux=((date_noi & 0b00010000) << 3)^aux; 
         aux=((date_noi & 0b00001000) << 4)^aux; 
         aux=((date_noi & 0b00000100) << 5)^aux;
         aux=((date_noi & 0b00000010) << 6)^aux;
         aux=((date_noi & 0b00000001) << 7)^aux;  
        if ((date_noi&0b10000000)==aux)
        {         
          ind=ind+1;
          if(ind == 1)
          {
          vStg_tmp=long(date_noi&0b01111111);
          
             if(vStg_tmp < 63)
             {  
                vStg=(63-vStg_tmp)*1040;
                pwmWrite(PA9,0); //max 65535 la 549.1Hz
                pwmWrite(PA8,vStg); //max 65535 la 549.1Hz
             }  else {
                vStg=(vStg_tmp-63)*1040;
                pwmWrite(PA9,vStg); //max 65535 la 549.1Hz
                pwmWrite(PA8, 0); //max 65535 la 549.1Hz
             } 
          } else {
          vDr_tmp=long(date_noi&0b01111111); 
           if(vDr_tmp < 63)
             {
                vDr=(63-vDr_tmp)*1040;
                pwmWrite(PB6,vDr); //max 65535 la 549.1Hz
                pwmWrite(PB7,0); //max 65535 la 549.1Hz
             } else {
                vDr=(vDr_tmp-63)*1040;
                pwmWrite(PB6,0); //max 65535 la 549.1Hz
                pwmWrite(PB7,vDr); //max 65535 la 549.1Hz
             }
          ind = 0;
        } 
       }
    }
  
  }
   delay(1);
}
