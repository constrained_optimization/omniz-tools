function [temp,tt]=plot_bspline_curve(b,knot,tt,varargin)

% also 3D; by default don't plot when it is >3D

% sample the time interval
if length(tt)==2
    tt=linspace(tt(1), tt(2),200);
end

% since internally we have stored the bsplines in cells, one per each
% interval, we need to do the plotting piece by piece

doPlot=1;
hold on; grid on

temp=[];

for jj=1:length(knot)-1
    % if the knot values are equal there is nothing to plot
    if knot(jj)==knot(jj+1)
        continue;
    end
    % get the time sampling which corresponds to the current interval (the
    % jj-th)
    tj=tt(logical((tt<=knot(jj+1)).*(tt>=knot(jj))));
    
    % plot the d-1 order bspline and the combination of d-order bsplines on
    % that interval
    temp=[temp double(subs(b{jj},tj))];
end

if doPlot==1
    if size(temp,1)==1
        warning 'the bspline curve is scalar-valued; it cannot be plotted (try instead to plot it against the time axis'
    elseif size(temp,1)==2        
        plot(temp(1,:),temp(2,:),'b','LineWidth',2,varargin{:});
    elseif size(temp,1)==3        
        plot3(temp(1,:),temp(2,:),temp(3,:),'b','LineWidth',2,varargin{:});
    else
        warning 'the bspline curve has more than 3 components and cannot be plotted; the projection obtained by taking the first 3 components is plotted'
        plot3(temp(1,:),temp(2,:),temp(3,:),'b','LineWidth',2,varargin{:});
        view(3)
    end
end
