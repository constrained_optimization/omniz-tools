function greville_knot=get_greville_points(knot,p)

greville_knot=[];
m=length(knot);
for j=1:m-p-1
    greville_knot(j)=sum(knot(j+1:j+p))/p;
end