clear all; clc; close all

T=200;                  % total length of the knot vector
tt=[0 1]*T;             % interval over which we plot
%% definition of the bpline basis functions
% bspline degree
p=4;            
% number of basis functions of order p
n=14;
% define the knot vector such that I corresponds to a clamped curve (first
% p+1 and last p+1 points repeat and the internal ones are uniformly
% distributed)
knot=[zeros(1,p+1) linspace(1/(n-p),(n-p-1)/(n-p),n-p-1) ones(1,p+1)]*T;
% generation of the bspline basis functions (from order 0 to order p); some
% of the initial and terminal knot interval are of zero-length; while they
% could be simplified, we leave them to siplify the representation (for the
% p order bspline there are m knot interval)
[b,knot]=bsplines(knot,p);
% total length of the knot vector; m,np have to respect relation n+p+1=m
m=length(knot);

%% control points

% control points obtained through a method or another; each control point
% is associated to a bspline function (hence number of control points has
% to be equal with the number of splines)
P =[ -600.0000 -489.8397 -230.2019   61.8447  234.5338 -532.6120 -382.9841  382.9841  532.6120 -234.5340  -61.8420  230.1829  489.9111  600.0220;...
      300.0000  323.3078  234.1254  500.0866 -491.4679 -240.2778  349.1161  349.1162 -240.2779 -491.4678  500.0864  234.1269  323.3026  299.9953];

bcurve=make_bspline_curve(b{end},P);

%% 
close all

plot_bsplines(b{end},knot,tt,'Color','b');

figure; hold on; grid on
plot_bspline_curve(bcurve,knot,tt,'Color','b');
scatter(P(1,:),P(2,:),'r','filled')
plot(P(1,:),P(2,:),'r')

%%
figure
plot_bsplines(b{end}{1},knot,tt,'Color','b');