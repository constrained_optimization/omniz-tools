% function P = generateClutteredEnvironmentV(N)
N=10;

% the number of obstacles to be generated
dim=2;                              % dimension in which the
bound=10;                           % bound for the limits
a=kron(ones(1,dim),[-1 1]*bound);   % axis limits
minG = 6;                           % in vertex representation, min number of vertices
maxG = 8;                          % in vertex representation, max number of vertices

showFigure = 1;                     % plot the results
noIntersection=1;

box=Polyhedron.unitBox(dim)*bound;

noGenerators = randi([minG maxG],1,N); % no generators for each of the polyhedra (the real number may be smaller since not all vertices are external)
scaling = minScaling + rand(1,N)*(maxScaling-minScaling);

% G=rand(dim,noGenerators);
% G=eye(dim);
G=rand(dim,4);

% generate random points within the box and construct the voronoi diagram
center=zeros(dim,N);
for i=1:dim
    center(i,:)=-bound+2*bound*rand(1,N);
end

[v,c]=voronoin(center');

% https://www.mathworks.com/matlabcentral/answers/258667-vertices-of-polygons-in-bounded-voronoi-diagram

[A,b,vert]=voronoiPolyhedrons(center,a(1:2:end),a(2:2:end));

vcells=[];
for i=1:N
    vcells=[vcells Polyhedron(A{i},b{i})];
end

Z=[];
for i=1:N    
    tmp=rand(size(vert{i},2),1);
    tmp=tmp./repmat(sum(tmp,1),size(tmp,1),1);
    
    zoncen=vert{i}*tmp;
    
    scaling=1/max(sum(abs(A{i}*G),2)./(b{i}-A{i}*zoncen));
    
    Z=[Z zonotope([zoncen G*scaling])];
end

% each Voronoi cell will have a single polyhedron
if showFigure
    plot(vcells,'Color','b','Alpha',.4)
    hold on; grid on
    scatter(center(1,:),center(2,:),'filled','r')
    for i=1:length(Z)
        plot(Z(i));
    end
end
% 
axis(a)