% function P = generateClutteredEnvironmentV(N)
N=3;

% the number of obstacles to be generated
dim=2;                              % dimension in which the
bound=10;                           % bound for the limits
a=kron(ones(1,dim),[-1 1]*bound);   % axis limits
minV = 10;                           % in vertex representation, min number of vertices
maxV = 12;                          % in vertex representation, max number of vertices

showFigure = 1;                     % plot the results
noIntersection=1;

box=Polyhedron.unitBox(dim)*bound;

noVertices = randi([minV maxV],1,N); % no vertices for each of the polyhedra (the real number may be smaller since not all vertices are external)
% scaling = minScaling + rand(1,N)*(maxScaling-minScaling);

% generate random centers, 1st method is to make convex combination of the
% box; it does not appear to work well
tmp=rand(2^dim,N);
tmp=tmp./sum(tmp,1);
center=box.V'*tmp;
% will try a 2nd method
center=zeros(dim,N);
for i=1:dim
    center(i,:)=-bound+2*bound*rand(1,N);
end

[v,c]=voronoin(center');

% https://www.mathworks.com/matlabcentral/answers/258667-vertices-of-polygons-in-bounded-voronoi-diagram

[A,b,vert]=voronoiPolyhedrons(center,a(1:2:end),a(2:2:end));

vcells=[];
for i=1:N
    vcells=[vcells Polyhedron(A{i},b{i})];
end

P=[];
Pinit=[];
for i=1:N    
    tmp=rand(size(vert{i},2),noVertices(i));
    tmp=tmp./repmat(sum(tmp,1),size(tmp,1),1);
    
    vertices=vert{i}*tmp;
    cheby=Polyhedron(vertices').chebyCenter.x;

    scaling=1/max(max((A{i}*(vertices-repmat(cheby,1,size(vertices,2)))./repmat(b{i}-A{i}*cheby,1,size(vertices,2)))));
    scaling=0.9*scaling;
    
    vertices=repmat(cheby,1,size(vertices,2))+scaling*(vertices-repmat(cheby,1,size(vertices,2)));
    P=[P Polyhedron(vertices')];
end

% each Voronoi cell will have a single polyhedron
if showFigure
    plot(vcells,'Color','b','Alpha',.4)
    hold on; grid on
    scatter(center(1,:),center(2,:),'filled','r')
    plot(P,'Color','r','Alpha',.4)
end
% 
axis(a)