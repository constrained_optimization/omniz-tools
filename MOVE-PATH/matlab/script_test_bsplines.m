clear all; clc; close all

T=200;                  % total length of the knot vector
tt=[0 1]*T;             % interval over which we plot
%% definition of the bpline basis functions
% bspline degree
p=4;            
% number of basis functions of order p
n=14;
% define the knot vector such that I corresponds to a clamped curve (first
% p+1 and last p+1 points repeat and the internal ones are uniformly
% distributed)
knot=[zeros(1,p+1) linspace(1/(n-p),(n-p-1)/(n-p),n-p-1) ones(1,p+1)]*T;
% generation of the bspline basis functions (from order 0 to order p); some
% of the initial and terminal knot interval are of zero-length; while they
% could be simplified, we leave them to siplify the representation (for the
% p order bspline there are m knot interval)
[b,knot]=bsplines(knot,p);
% total length of the knot vector; m,np have to respect relation n+p+1=m
m=length(knot);

% % definition of the bspline basis functions used for approximation
% p_approx=4;
% n_approx=29;
% knot_approx=[zeros(1,p_approx+1) linspace(1/(n_approx-p_approx),(n_approx-p_approx-1)/(n_approx-p_approx),n_approx-p_approx-1) ones(1,p_approx+1)]*T;
% [b_approx,knot_approx]=bsplines(knot_approx,p_approx);
% m_approx=length(knot_approx);

%% control points

% control points obtained through a method or another; each control point
% is associated to a bspline function (hence number of control points has
% to be equal with the number of splines)
P =[ -600.0000 -489.8397 -230.2019   61.8447  234.5338 -532.6120 -382.9841  382.9841  532.6120 -234.5340  -61.8420  230.1829  489.9111  600.0220;...
      300.0000  323.3078  234.1254  500.0866 -491.4679 -240.2778  349.1161  349.1162 -240.2779 -491.4678  500.0864  234.1269  323.3026  299.9953];
  
% we define time instants for further knot refinement; while not strictly
% necessary to do so we simply divide each of the original knot's non-empty
% intervals into 4 new sub-intervals
sigma=unique([...
    knot(1:end-1)+(knot(2:end)-knot(1:end-1))/4 ...
    knot(1:end-1)+2*(knot(2:end)-knot(1:end-1))/4 ...
    knot(1:end-1)+3*(knot(2:end)-knot(1:end-1))/4 ...
    ]);
% to avoid issues we delete the points which are already part of the knot
sigma(find(ismember(sigma,knot)))=[];

% define the new control points (Q) and knot vector (knotQ), alphaQ
% describes the weights which allow to obtain Q from the initial P; P can
% be either double or sdpvar variables
[Q,alphaQ,knotQ]=knot_insertion_to_bspline_curve(P,knot,p,sigma);

% construct the bspline basis associated with the knot refinement
mQ=length(knotQ);
nQ=mQ-p-1;
[bQ,knotQ]=bsplines(knotQ,p);

% bspline curve based on control points P
bcurve=make_bspline_curve(b{end},P);
% bspline curve based on control points Q, obtained through knot refinement
bcurveQ=make_bspline_curve(bQ{end},Q);

% variables and splines for the first derivative of the bspline curve

% control points for the derived bspline curve
Pdot=p*(P(:,2:end)-P(:,1:end-1))./repmat(knot(p+2:n+p)-knot(2:n),size(P,1),1);
% refined control points for the derived bspline curve
Qdot=p*(Q(:,2:end)-Q(:,1:end-1))./repmat(knotQ(p+2:nQ+p)-knotQ(2:nQ),size(Q,1),1);
% derived bspline curve, based on control points Pdot; since we work on the
% original knot we have to consider the n-1 relevant ones (the ones from 2 to n)
bcurve_dot=make_bspline_curve({b{end-1}{2:n}},Pdot);

% variables and splines for the second order derivative of the bspline curve

% control points for the second order derived bspline curve
Pddot=(p-1)*(Pdot(:,2:end)-Pdot(:,1:end-1))./repmat(knot(p+2:n+p-1)-knot(3:n),size(Pdot,1),1);
% refined control points for the second order derived bspline curve
Qddot=(p-1)*(Qdot(:,2:end)-Qdot(:,1:end-1))./repmat(knotQ(p+2:nQ+p-1)-knotQ(3:nQ),size(Qdot,1),1);
% derived bspline curve, based on control points Pddot; since we work on the
% original knot we have to consider the n-2 relevant ones (the ones from 3 to n)
bcurve_ddot=make_bspline_curve({b{end-2}{3:n}},Pddot);

% for various nonlinear bspline curves we need products of bspline
% functions (tensor products); we define the resulting bspline products and
% their associated control points; 

% in particular, we will need dot x dot and dot ddot products
% the first output is the collection of bspline products and the second
% argument gives the combination of indices 
[b_d_d,ind_d_d]=multiply_bsplines({b{end-1}{2:n}},{b{end-1}{2:n}});
% the associated control points
P_d_d=diag(Pdot(:,ind_d_d(:,1))'*Pdot(:,ind_d_d(:,2)));
% the resulting tensor b-spline curve
bcurve_d_d=make_bspline_curve(b_d_d,P_d_d);

% the same but for the product d x dd
[b_d_dd,ind_d_dd]=multiply_bsplines({b{end-1}{2:n}},{b{end-2}{3:n}});
P_d_dd=diag(Pdot(:,ind_d_dd(:,1))'*[0 1;-1 0]*Pddot(:,ind_d_dd(:,2)));
bcurve_d_dd=make_bspline_curve(b_d_dd,P_d_dd);

% define nonlinear functions based on spline products

% velocity magnitude Va
for i=1:length(bcurve_d_d)
    bcurve_va{i}=vpa(sqrt(bcurve_d_d{i}),5);
end

% for further use, compute the second order derivative of Va
for i=1:length(bcurve_va)    
    if logical(bcurve_va{i}==0)
        bcurve_va_dd{i}=sym(0);
    else
        bcurve_va_dd{i}=vpa(collect(simplify(diff(diff(bcurve_va{i})))),3);
    end
end


% banking angle phi
for i=1:length(bcurve_d_d)
    if double(bcurve_d_d{i}~=0)
        bcurve_phi{i}=vpa(bcurve_d_dd{i}/sqrt(bcurve_d_d{i}),5);
    else
        bcurve_phi{i}=sym(0);
    end
end

% for further use, compute the second order derivative of phi
for i=1:length(bcurve_phi)    
    if logical(bcurve_phi{i}==0)
        bcurve_phi_dd{i}=sym(0);
    else
        bcurve_phi_dd{i}=vpa(collect(simplify(diff(diff(bcurve_phi{i})))),3);
    end
end

% for approximating the nonlinear functions with the Schoenberg operator we
% don't have, necesarilly to use the same knot vector and bspline basis, we
% may decide on other representations; here we alternate between the
% orignal representation and the refined one

% knot_approx=knot;
% n_approx=n;
% p_approx=p;
% b_approx=b;

knot_approx=knotQ;
n_approx=nQ;
p_approx=p;
b_approx=bQ;

% compute the Greville points for the refined knot
greville_knot=get_greville_points(knot_approx,p_approx);

% construct quasi-interpolant for Va, based on the Schoenberg operator
P_va_approx=eval_bspline(bcurve_va,knot,greville_knot);
bcurve_va_approx=make_bspline_curve(b_approx{end},P_va_approx);
[temp_va_approx,tt_va_approx]=plot_bspline_curve(bcurve_va_approx,knot_approx,tt);

% construct quasi-interpolant for phi, based on the Schoenberg operator
P_phi_approx=eval_bspline(bcurve_phi,knot,greville_knot);
bcurve_phi_approx=make_bspline_curve(b_approx{end},P_phi_approx);
[temp_phi_approx,tt_phi_approx]=plot_bspline_curve(bcurve_phi_approx,knot_approx,tt);

%% bspline plotting, the nominal and the refined version
close all

plot_bsplines(b{end},knot,tt,'Color','b');
plot_bsplines(bQ{end},knotQ,tt,'Color','r');

%% plot the bspline curve with the union of convex hull containing it (the refined version which gives a tighter approximation)
figure; hold on; grid on

% plot the bspline curve and the union of convex hulls constraining it
% (based on the refined knot vector)
for i=1:nQ-p
    plot(Polyhedron(Q(:,i:i+p)'),'Color','b','Alpha',.2)
end
temp=plot_bspline_curve(bcurve,knot,tt,'Color','r');

%% plot the first derivative of the bpline curve and its bounding convex hulls
figure; grid on; hold on

% plot the first order derived bspline curve and the union of convex hulls constraining it
% (based on the refined knot vector)
for i=1:nQ-p
    plot(Polyhedron(Qdot(:,i:i+p-1)'),'Color','b','Alpha',.2)
end
temp_dot=plot_bspline_curve(bcurve_dot,knot,tt,'Color','r');

%% plot the second derivative of the bpline curve and its bounding convex hulls
figure; grid on; hold on

% plot the second order derived bspline curve and the union of convex hulls constraining it
% (based on the refined knot vector)
for i=1:nQ-p
    plot(Polyhedron(Qddot(:,i:i+p-2)'),'Color','b','Alpha',.2)
end
temp_ddot=plot_bspline_curve(bcurve_ddot,knot,tt,'Color','r');

%% plotting for Va and phi; the exact forms and the qusi-interpolant obtained by the application of the Schoenberg operator
% plotting for Va
figure; grid on; hold on

plot_bspline_1D_bound(P_va_approx,knot_approx,n_approx,p_approx,'r','FaceAlpha',.2);

[temp_va,tt_va]=plot_bspline_curve(bcurve_va,knot,tt);
plot(tt_va,temp_va,'LineWidth',2)
plot(tt_va_approx,temp_va_approx,'r','LineWidth',2)

aa=axis;
plot(aa([1 2]),min((P_va_approx))*[1 1],'r--','LineWidth',1)
plot(aa([1 2]),max((P_va_approx))*[1 1],'r--','LineWidth',1)

title('V_a')

% plotting for phi
figure; grid on; hold on

plot_bspline_1D_bound(P_phi_approx,knot_approx,n_approx,p_approx,'r','FaceAlpha',.2);

[temp_phi,tt_phi]=plot_bspline_curve(bcurve_phi,knot,tt);
plot(tt_phi,temp_phi,'LineWidth',2)
plot(tt_phi_approx,temp_phi_approx,'r','LineWidth',2)

aa=axis;
plot(aa([1 2]),min(P_phi_approx)*[1 1],'r--','LineWidth',1)
plot(aa([1 2]),max(P_phi_approx)*[1 1],'r--','LineWidth',1)

title('$\mathbf{\phi}$','Interpreter','latex')

%% theoretical bounds for the qusi-interpolant error

% get the maximum knot interval size
h_knot=max(knot_approx(p_approx+2:n_approx+1)-knot_approx(p_approx+1:n_approx));

% plot the second order derivative of the Va nonlinear curve and get the
% inf norm (the max absolute value) from which compose the actual bound
[temp_va_dd,temp_t_va_dd]=plot_bsplines(bcurve_va_dd,knot,tt);

D2_Va=max(abs(temp_va_dd));

figure; grid on; hold on
plot(temp_t_va_dd,temp_va_dd)
title(['double derivative of V_a with infinity norm=' num2str(D2_Va)])

disp(['the infinity bound is ' num2str(2*((2*p+1)^2)*(h_knot^2)*D2_Va)])

% plot the second order derivative of the Va nonlinear curve and get the
% inf norm (the max absolute value) from which compose the actual bound
[temp_phi_dd,temp_t_phi_dd]=plot_bsplines(bcurve_phi_dd,knot,tt);

D2_phi=max(abs(temp_phi_dd));

figure; grid on; hold on
plot(temp_t_phi_dd,temp_phi_dd)
title(['double derivative of \phi with infinity norm=' num2str(D2_phi)])

disp(['the infinity bound is ' num2str(2*((2*p+1)^2)*(h_knot^2)*D2_phi)])

%% plot second order smoothness moduli (for use in a sharper bound)
figure; grid on; hold on
for h=linspace(0.01,10,10)
    tt_h=linspace(tt(1)+h,tt(end)-h,200);
    temp=eval_bspline(bcurve_va,knot,tt_h-h)...
        +eval_bspline(bcurve_va,knot,tt_h+h);
    if length(temp)>200
        temp=temp(1:200);
    end
    temp=temp-2*eval_bspline(bcurve_va,knot,tt_h);
    plot(tt_h,temp)
    drawnow
end

figure; grid on; hold on
for h=linspace(0.01,10,10)
    tt_h=linspace(tt(1)+h,tt(end)-h,200);
    temp=eval_bspline(bcurve_phi,knot,tt_h-h)...
        +eval_bspline(bcurve_phi,knot,tt_h+h);
    if length(temp)>200
        temp=temp(1:200);
    end
    temp=temp-2*eval_bspline(bcurve_phi,knot,tt_h);
    plot(tt_h,temp)
    drawnow
end

