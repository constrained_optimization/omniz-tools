//ptr bluetooth: 5V la VCC si Gnd la Gnd
//ptr bluetooth: Rx_bluetooth=>Tx3(PB10) || Tx_bluetooth=>Rx3(PB11)
//###############################################################
//ptr com seriala cu slaves: PA2(Tx2) PA3(Rx2)
//##############################################################
// PerecheR1=vStg  alim  PerecheR2=vDr
// PerecheR3=vStg        PerecheR4=vDr

char datele1=0, datele2=0, datele3=0;
byte aux=0;
byte vStg=63, vDr=63; //vitezele la perechi de roti 
//Valori 63=0rpm 0=-504rpm 127=+504rpm, (de la 0 la 127 din 1 in 1)
//63 sta pe loc, ce e sub o ia inapoi, ce e peste inainte
//PB9 pinul pentru frane
unsigned long t1=0,t2=0;
byte stare=0; //cand stare=0, se ridica stepperele
//###variabile pentru tone
unsigned long timp1=0, timp2=0, timp3=0, timp4=0;
int ind_t1=0, ind_t2=0, ind_t3=0, ind_t4=0;
int stare_t1=LOW, stare_t2=LOW, stare_t3=LOW, stare_t4=LOW;
//VAR GLOBALA PENTRU URCAT
int urcat = LOW;
void setup() 
{
 pinMode(PC13,OUTPUT);  //led de pe placa
 pinMode(PB9, OUTPUT);  //frane
  //pentru driver stanga spate pas cu pas
 pinMode(PB12, OUTPUT); //PUL+
 pinMode(PB13, OUTPUT); //DIR+
 pinMode(PB14, OUTPUT); //ENA+
 //pentru driver stanga fata
 pinMode(PB15, OUTPUT); //PUL+
 pinMode(PA8, OUTPUT); //DIR+
 pinMode(PA9, OUTPUT); //ENA+
 //pentru driver dreapta spate
 pinMode(PA6, OUTPUT); //PUL+
 pinMode(PA5, OUTPUT); //DIR+
 pinMode(PA4, OUTPUT); //ENA+
 //pentru driver dreapta fata
 pinMode(PB5, OUTPUT); //PUL+
 pinMode(PB6, OUTPUT); //DIR+
 pinMode(PB7, OUTPUT); //ENA+
 Serial3.begin(9600); //default speed of bluetooth rx3-PB11 PB10 -tx3
 Serial2.begin(57600); //ptr a comunica cu sclavetii: PA2(Tx2) PA3(Rx2)
 digitalWrite(PB9,LOW); //franele sunt libere
 digitalWrite(PB14, HIGH); //dezactivam enable
 digitalWrite(PA9, HIGH); //dezactivam enable
 digitalWrite(PA4, HIGH); //dezactivam enable
 digitalWrite(PB7, HIGH); //dezactivam enable
 
}


void loop() {
  digitalWrite(PC13,HIGH);
  while (Serial3.available() > 0) 
  {
  // read the incoming byte:
    datele1 = Serial3.read();
    datele2 = Serial3.read(); //cr
    datele3 = Serial3.read(); //lf   
    if (datele1=='x')
    {
     vStg=63; vDr=63;  //oprim tot. ce e sub o ia inapoi, ce e peste o ia inainte
     digitalWrite(PB9, LOW); //franele sunt active(libere)
     }
    else if (datele1=='w')
    { 
     if (vStg!=127) {vStg=vStg+3;}
     if (vDr!=127) {vDr=vDr+3;}
    }
    else if (datele1=='s')
    {
     if (vStg!=0) {vStg=vStg-3;}
     if (vDr!=0) {vDr=vDr-3;}
    }
    else if (datele1=='a')
    {
     if (vStg!=0) {vStg=vStg+3;}
     if (vDr!=127) {vDr=vDr-3;}
    }
    else if (datele1=='d')
    {
     if (vStg!=127) {vStg=vStg-3;}
     if (vDr!=0) {vDr=vDr+3;}
    }
    else if (datele1 == 'u')
    {
      stare = 1;
    }
    else if (datele1 == 'c')
    {
      stare = 3;
    }
    if ((datele1=='w')||(datele1=='s')||(datele1=='a')||(datele1=='d')||(datele1=='x'))
    {
     //trimitem catre clienti starea de urcat sau coborat
    if(urcat == HIGH) 
    {
      Serial2.write(0b10000011);
    } else {
      Serial2.write(0b10000000);
    }
     aux=(vStg & 0b01000000) << 1; 
     aux=((vStg & 0b00100000) << 2)^aux; 
     aux=((vStg & 0b00010000) << 3)^aux; 
     aux=((vStg & 0b00001000) << 4)^aux; 
     aux=((vStg & 0b00000100) << 5)^aux;
     aux=((vStg & 0b00000010) << 6)^aux;
     aux=((vStg & 0b00000001) << 7)^aux;   
     Serial2.write(aux|vStg);//trimitem catre perechile din stanga
     aux=(vDr & 0b01000000) << 1; 
     aux=((vDr & 0b00100000) << 2)^aux; 
     aux=((vDr & 0b00010000) << 3)^aux; 
     aux=((vDr & 0b00001000) << 4)^aux; 
     aux=((vDr & 0b00000100) << 5)^aux;
     aux=((vDr & 0b00000010) << 6)^aux;
     aux=((vDr & 0b00000001) << 7)^aux;  
     Serial2.write(aux|vDr);//trimitem catre perechile din dreapta
     Serial3.println(aux);
    }      
  }
  t2=millis();      //la secunda juma anuntam pe bluetooth ca totul este ok
  if (t2-t1>1500)   //la secunda juma anuntam pe bluetooth ca totul este ok
  {    
     Serial3.println("totul ok"); //trimitem catre bluetooth
     t1=t2;
  }
  if(stare == 1)
  {
    //delay(500);
    //pentru stanga spate
    digitalWrite(PB9, LOW); //franele sunt active(libere) 
    
    digitalWrite(PB14, LOW); //activam enable
    digitalWrite(PB13, HIGH); //directie
    //tone(PB12, 1000, 800); //pul
    ind_t1=2200;
  
    //pentru stanga fata
    digitalWrite(PA9, LOW); //activam enable
    digitalWrite(PA8, LOW); //directie
    //tone(PB15, 1000, 800); //pul
    ind_t2=2200;
    
    //pentru dreapta spate
    digitalWrite(PA4, LOW); //activam enable
    digitalWrite(PA5, LOW); //directie
    //tone(PA10, 1000, 800); //pul
    ind_t3=2200;
    
    //pentru dreapta fata
    digitalWrite(PB7, LOW); //activam enable
    digitalWrite(PB6, HIGH); //directie
    //tone(PA15, 1000, 800); //pul
    ind_t4=2200;
    
    delay(400);
    urcat = HIGH; 
    stare = 2;
  }

    if(stare == 3)
  {
    //delay(500);
    digitalWrite(PB9, LOW); //franele sunt active(libere) 

//    //pentru stanga spate
//    digitalWrite(PB14, LOW); //activam enable
//    digitalWrite(PB13, LOW); //directie
//    //tone(PB12, 1000, 800); //pul
//    ind_t1=2200;
  
    //pentru stanga fata
    digitalWrite(PA9, LOW); //activam enable
    digitalWrite(PA8, HIGH); //directie
    //tone(PB15, 1000, 800); //pul
    ind_t2=2200;
    
//    //pentru dreapta spate
//    digitalWrite(PA4, LOW); //activam enable
//    digitalWrite(PA5, HIGH); //directie
//    //tone(PA10, 1000, 800); //pul
//    ind_t3=2200;
    
    //pentru dreapta fata
    digitalWrite(PB7, LOW); //activam enable
    digitalWrite(PB6, LOW); //directie
    //tone(PA15, 1000, 800); //pul
    ind_t4=2200;
    
    delay(400);
    urcat = LOW; //urcat = LOW;    //LOW ADICA NU SE VA BLOCA LA FINAL FRANA
    stare = 2;
  }
  
  if(stare == 2) 
  {    
    //tone(PB12, 1000, 800);
    
//    //STANGA SPATE
//    if(ind_t1 != 0) 
//    {
//      if((micros()-timp1) >= 300)
//      {
//        stare_t1=!stare_t1;
//        timp1=micros();
//        ind_t1=ind_t1-1;
//        digitalWrite(PB12, stare_t1);
//      }
//    }

    //STANGA FATA
    if(ind_t2 != 0) 
    {
      if((micros()-timp2) >= 300)
      {
        stare_t2=!stare_t2;
        timp2=micros();
        ind_t2=ind_t2-1;
        digitalWrite(PB15, stare_t2);
      }
    }

//    //DREAPTA SPATE
//     if(ind_t3 != 0) 
//    {
//      if((micros()-timp3) >= 300)
//      {
//        stare_t3=!stare_t3;
//        timp3=micros();
//        ind_t3=ind_t3-1;
//        digitalWrite(PA6, stare_t3);
//      }
//    }

    //DREAPTA FATA
     if(ind_t4 != 0) 
    {
      if((micros()-timp4) >= 300)
      {
        stare_t4=!stare_t4;
        timp4=micros();
        ind_t4=ind_t4-1;
        digitalWrite(PB5, stare_t4);  
      }
    }

    if(ind_t1 == 0 && ind_t2 == 0 && ind_t3 == 0 && ind_t4 == 0)
    {
      //delay(1000);
      digitalWrite(PC13,  HIGH);
      digitalWrite(PB14, HIGH); //dezactivam enable
      digitalWrite(PA9, HIGH); //dezactivam enable
      digitalWrite(PA4, HIGH); //dezactivam enable
      digitalWrite(PB7, HIGH); //dezactivam enable
      digitalWrite(PB9, urcat); //franele: daca urcat=HIGH, franele se blocheaza; altfel, sunt libere
      stare = 0;
    }
    
  }

  
}
