%get integral of the tensor b1^T*b2 with the knot vector given
function [M,M_splines]=get_integral_bspline(b,knot,tt)

if nargin<3
    tt=[knot(1) knot(end)];
end

nb=length(b);

M=0;


for jj=find(tt(1)>=knot,1,'last'):find(tt(end)<=knot,1,'first')-1
    % if the knot values are equal there is nothing to plot
    M_splines(jj)=0;
    if knot(jj)==knot(jj+1)
        continue;
    end
    tmin=max(tt(1),knot(jj));
    tmax=min(tt(end),knot(jj+1));

    M_splines(jj)=vpa(int(b{jj},tmin,tmax),5);
    M=M+M_splines(jj);
end
% TO DO: compute definite integral from sqrt: 