n = 4528;
ip = '127.0.0.1';

% Parameters
plot_update_interval=20; % Frame interval for update of plot

% ### Connect to QTM
QCM('connect', ip, 'frameinfo', '3D'); % Connects to QTM and keeps the connection alive.


% Initialize figure
% - Figure parameters
nax=3;
nmarkers=5;
nframes=n;

% - Plot data buffer
ydata=nan(nframes,nmarkers,nax);


% State variables
skipped_frames=0;
last_frame=nan;

% Real time loop
for i_frame = 1:n
    % ### Fetch data from QTM
    [frameinfo,frame_3d] = QCM;
    
    current_frame=frameinfo(1);
    shift=current_frame-last_frame; % Calculate shift (1 frame, if no skipped frames)
    
    % Update data buffer
    ydata(1:nframes-shift,:)=ydata(1+shift:nframes,:); % Shift data to the left
    ydata(nframes,:,:)=frame_3d(1:nmarkers,:); % Add current value
    if shift>1
        ydata(nframes-shift+1:nframes-1,:,:)=nan; % Set skipped frames to NaN (if any).
    end
    
    % Update state (count missed frames, loop time exceeds frame rate)
    % Tip: use multithread processing when you have the Matlab parallel
    % computing toolbox. One thread for updating buffer (RT communication)
    % and another for processing and plotting.
    skipped_frames=skipped_frames+shift-1; % Count skipped frames
    last_frame=current_frame;
end

% Report skipped frames
fprintf('Skipped frames: %u/%d\n',skipped_frames,n);

% Terminate QMC object and clear
QCM('disconnect');
clear mex

%% data plotting

close all
nframes=2700
xy_position=nan(2,nframes);
z_offset=nan(1,nframes);
for i=1:nframes
    xy_position(:,i)=sum(reshape(ydata(i,:,1:2),nmarkers,2),1)/nmarkers;
    z_offset(:,i)=sum(reshape(ydata(i,:,3),nmarkers,1),1)/nmarkers;
end

figure
subplot(3,4,1:8); grid on; hold on
plot(xy_position(1,:),xy_position(2,:),'*-')
xlabel('x'); ylabel('y'); title('deplasare în plan (x-y)')

subplot(3,4,9:12); grid on; hold on
plot(z_offset)
xlabel('cadru imagine'); ylabel('z'); title('deplasare pe verticală (axa z)')