function [temp,tt]=plot_bsplines(b,knot,tt,varargin)

% also 3D; by default don't plot when it is >3D

% sample the time interval
if length(tt)==2
    tt=linspace(tt(1), tt(2),200);
end

% since internally we have stored the bsplines in cells, one per each
% interval, we need to do the plotting piece by piece

btt=zeros(2,length(tt));

isBasis=iscell(b{1});

if nargout==0
    doPlot=1;
    figure; hold on; grid on
else
    doPlot=0;
end

if isBasis
    for i=1:length(b)
        temp{i}=0;
    end
else
    temp=[];
end

j=1;
for jj=1:length(knot)-1
    % if the knot values are equal there is nothing to plot
    if knot(jj)==knot(jj+1)
        continue;
    end
    % get the time sampling which corresponds to the current interval (the
    % jj-th)
    tj=tt(logical((tt<=knot(jj+1)).*(tt>=knot(jj))));
    
    % plot the d-1 order bspline and the combination of d-order bsplines on
    % that interval
    if isBasis
        for i=1:length(b)
            temp{i}=[temp{i} double(subs(b{i}{jj},tj))];
        end
    else
        temp=[temp double(subs(b{jj},tj))];
    end
end

if doPlot==1
    if isBasis
        for i=1:length(b)
            plot(tt,temp{i}(2:end),varargin{:});
        end
    else
        plot(tt,temp,varargin{:});
    end
end
