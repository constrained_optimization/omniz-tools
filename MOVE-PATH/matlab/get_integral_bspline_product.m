%get integral of the tensor b1^T*b2 with the knot vector given
function [M]=get_integral_bspline_product(b1,b2,knot,pd,tt)

if nargin<5
    tt=[knot(1) knot(end)];
end

nb1=length(b1);
nb2=length(b2);

M=zeros(nb1,nb2);

for jj=1:length(knot)-1
    % if the knot values are equal there is nothing to plot
    if knot(jj)==knot(jj+1)
        continue;
    end
    
    if knot(jj+1)<tt(1)
        continue;
    end
    if knot(jj)>tt(end)
        continue;
    end

    if knot(jj)<=tt(1) && tt(1)<knot(jj+1)
        tmin=tt(1);
    else
        tmin=knot(jj);
    end
    if knot(jj)<=tt(end) && tt(end)<knot(jj+1)
        tmax=tt(end);
    else
        tmax=knot(jj+1);
    end
   

%   the i-th bspline is non-empty only on the interval [i i+p+1] which
%   means that for the interval t_jj, t_{jj+1} the non-empty splines are
%   those with indices from jj-p to jj

    for i=jj-pd-1:jj-1%1:nb1
        for j=jj-pd-1:jj-1%1:nb2
            M(i,j)=M(i,j)+int(b1{i}{jj}*b2{j}{jj},tmin,tmax);
        end
    end
end
% TO DO: compute definite integral from sqrt: vpa(int(sqrt(b1{i}{jj}*b2{j}{jj}),knot(jj),knot(jj+1)),5) 