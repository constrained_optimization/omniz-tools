% implementation is based on Lyche, Tom, Carla Manni, and Hendrik Speleers. "Foundations of spline theory: B-splines, spline approximation, and hierarchical refinement." Splines and PDEs: From Approximation Theory to Numerical Linear Algebra. Springer, Cham, 2018. 1-76

% input data: knot vector xi, degree p
% output data: b returns a 3-level cell array: b{k}{i}{j} is the value of the k-th
% degree i-th bspline on the j-th knot interval (from knot(j) to knot(j+1))

% in this way we keep simple (polynomial formulations) on each interval
% and later on the computations are simpler

function [b,xi]=bsplines(xi,p)

xi=sort(real(xi));  % the knot vector has to be sorted monotonically increasing
m=length(xi);       % number of elements in the knot vector

if m<p+2
    error 'the knot size (m) and the degree (p) have to respect relation m >= p+2'
end

syms t
b={};

% In Matlab we cannot index with zero; so the first index in the
% multi-level cell array has to be read as 'degree=index-1'

% initialize 0-order bsplines with zero everywere (below eq. (2.1) at page 4)
k=1;
for i=1:m-1
    for j=1:m
        b{k}{i}{j}=0;
    end
end
% make 1 the xi interval corresponding to the i-th bspline 0-degree
% bspline
for i=1:m-1
    if xi(i)<xi(i+1)
        b{k}{i}{i}=1;
    end
end   

% apply the recursive bpsline relation (eq. (2.1) at page 4)
for k=2:p+1
    for i=1:m-k
        for j=1:m
            b{k}{i}{j}=sym(0);
            if xi(i+k-1)-xi(i)~=0
                b{k}{i}{j}=b{k}{i}{j}+...
                    (b{k-1}{i}{j}.*(t-xi(i))./(xi(i+k-1)-xi(i)));
            end
            if (xi(i+k)-xi(i+1)~=0)
                b{k}{i}{j}=b{k}{i}{j}+...
                    (b{k-1}{i+1}{j}.*(xi(i+k)-t)./(xi(i+k)-xi(i+1)));
            end
        end
    end
end

% for each sub-interval, the bspline is a polynomial; apply collect to
% simplify the representation
for k=2:p+1
    for i=1:m-k
        for j=1:m
            b{k}{i}{j}=collect(b{k}{i}{j});
        end
    end
end

