function [bcurve]=make_bspline_curve(b,weights)


%get number of cells
m=length(b{1});
isArray=iscell(weights);

if isArray==0
    [n1,n2]=size(weights);
    if n1==1 || n2==1
        for i=1:m
            bcurve{i}=0;
            for j=1:length(b)
                bcurve{i}=bcurve{i}+b{j}{i}*weights(j);
            end
        end
    else
        for i=1:m
            bcurve{i}=0;
            for j=1:length(b)
                bcurve{i}=bcurve{i}+b{j}{i}*weights(:,j);
            end
        end
    end
else
    [n1,n2]=size(weights{1});
    if n1==1 || n2==1
        for i=1:m
            bcurve{i}=0;
            for j=1:length(b)
                bcurve{i}=bcurve{i}+b{j}{i}*weights{i}(j);
            end
        end
    else
        for i=1:m
            bcurve{i}=0;
            for j=1:length(b)
                bcurve{i}=bcurve{i}+b{j}{i}*weights{i}(:,j);
            end
        end
    end    
end

for i=1:length(bcurve)
    bcurve{i}=vpa(bcurve{i},3);
end
