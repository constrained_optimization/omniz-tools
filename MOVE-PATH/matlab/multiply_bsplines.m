function [bm,indices]=multiply_bsplines(b1,b2)

%assume that two bspline families are defined over the same knot vector;
%then the function returns the products between each elements of the
%families, over the same knot vector

m=length(b1{1});
ll=0;
indices=[];
for i=1:length(b1)
    for j=1:length(b2)
        ll=ll+1;
        indices=[indices; i j];
        for jj=1:m
            bm{ll}{jj}=b1{i}{jj}*b2{j}{jj};
        end
    end
end