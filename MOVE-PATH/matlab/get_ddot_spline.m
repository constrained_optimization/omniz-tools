% get ddot spline function

function ddot_spline=get_ddot_spline(bnum,bden)


for i=1:length(bnum)    
    if logical(bden{i}==0)
        ddot_spline{i}=sym(0);
    else
        ddot_spline{i}=vpa(collect(simplify(diff(diff(bnum{i}/bden{i})))),3);
    end
end