% function P = generateClutteredEnvironment(N)
N=10;

% N = the number of obstacles to be generated
dim=2;                              % dimension in which the
bound=10;                           % bound for the limits
a=kron(ones(1,dim),[-1 1]*bound);   % axis limits
minV = 3;                           % in vertex representation, min number of vertices
maxV = 10;                          % in vertex representation, max number of vertices
minScaling=5;                       % min scaling factor for the randomly generated Polyhedron
maxScaling=10;                      % max scaling factor for the randomly generated Polyhedron

showFigure = 1;                     % plot the results
noIntersection=1;

box=Polyhedron.unitBox(dim)*bound;

noVertices = randi([minV maxV],1,N); % no vertices for each of the polyhedra (the real number may be smaller since not all vertices are external)
scaling = minScaling + rand(1,N)*(maxScaling-minScaling);

% generate random centers, 1st method is to make convex combination of the
% box; it does not appear to work well
tmp=rand(2^dim,N);
tmp=tmp./sum(tmp,1);
center=box.V'*tmp;
% will try a 2nd method
center=zeros(dim,N);
for i=1:dim
    center(i,:)=-bound+2*bound*rand(1,N);
end

P=[];
for i=1:N
    P=[P Polyhedron(rand(noVertices(i),dim)-0.5)*scaling(i)];
    P(i)=P(i)+(-P(i).chebyCenter.x+center(:,i));
end
P=P & box;                          % bound the polytopes to stay within the box

if noIntersection
    % check for intersection between any pair of polytopes
    % M=zeros(length(P));
    % for i=1:length(P)-1
    %     for j=i+1:length(P)
    %         tmp=P(i) & P(j);
    %         M(i,j)=tmp.isEmptySet();
    %     end
    % end
    i=1;
    j=1;
    while (i<length(P)-1) && (j<length(P))
        flag=0;
        i=0;
        while (i<length(P)-1) && (flag==0)
            i=i+1;
            j=i;
            while (j<length(P)) && (flag==0)
                j=j+1;
                tmp=P(i) & P(j);
                if tmp.isEmptySet()==0
                    flag=1;
                    c1=P(i).chebyCenter.x;
                    c2=P(j).chebyCenter.x;
                    P(i)=c1+(P(i)-c1)*0.9;
                    P(j)=c2+(P(j)-c2)*0.9;
                    tmp=P(i) & P(j);
                    if tmp.isEmptySet()==0
                        j=j-1;
                    end
                end
            end
        end
    end
end

if showFigure
    plot(P,'Alpha',.4,'Color','b')
    hold on; grid on
end
% scatter(center(1,:),center(2,:),'filled','r')
axis(a)