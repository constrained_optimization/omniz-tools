function [b_eval_all]=eval_bspline(bb,knot,t_eval)

b_eval_all=[];
if iscell(bb{1})
    for i=1:length(bb)
        b=bb{i};
        b_eval=[];
        for jj=1:length(knot)-1
            % if the knot values are equal there is nothing to plot
            if knot(jj)==knot(jj+1)
                continue;
            end
            % get the time sampling which corresponds to the current interval (the
            % jj-th)
            tj=t_eval(logical((t_eval<=knot(jj+1)-1e-6).*(t_eval>=knot(jj)-1e-6)));
            b_eval=[b_eval double(subs(b{jj},tj))];            
        end
%         % particular limit case
%         if knot(jj+1)<=max(t_eval) && max(t_eval)<=knot(jj+1)+1e-6
%             b_eval=[b_eval double(subs(b{jj},max(t_eval)))];            
%         end
        b_eval_all=[b_eval_all; b_eval];
    end
else
    b=bb;
    b_eval=[];
    for jj=1:length(knot)-1
        % if the knot values are equal there is nothing to plot
        if knot(jj)==knot(jj+1)
            continue;
        end
        % get the time sampling which corresponds to the current interval (the
        % jj-th)
        tj=t_eval(logical((t_eval<=knot(jj+1)).*(t_eval>=knot(jj))));
        b_eval=[b_eval double(subs(b{jj},tj))];
    end
    b_eval_all=b_eval;
end