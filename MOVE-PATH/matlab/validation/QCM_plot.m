%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   QCMdemo_plot
%
%   Demo showing real time data of a rigid body (X, Y, Z, Yaw, Pitch, Roll)
%   in a rolling graph. The current frame is shown at t=0 and past data is
%   rolling to the left. The vertical axis position and scale automatically
%   adapt to the data.
%
%   Requires that a rigid body with name "Object" is present.
% 
%   You can use the prerecorded QTM file "6d_and_analog.qtm" and play
%   it in QTM with real time output (Shift + Ctrl + Space). Of course, you
%   can also perform this demo with QTM in preview mode.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function ydata=QCM_plot(n, ip, object)

% Function QMCdemo_plot(n, ip)
% 
% Input arguments:
% - n:  number of frames to be retrieved from QTM. Execution of the function
%       stops when the number of frames has been reached.
% - ip: The IP adress of the computer running QTM. If no IP is specified 
%       '127.0.0.1' (localhost) is used.

if (nargin < 2)
    ip = '127.0.0.1';
end
if (nargin < 3)
    object='Object';
end

% Parameters
plot_update_interval=20; % Frame interval for update of plot

% ### Connect to QTM
QCM('connect', ip, 'frameinfo', '6deuler'); % Connects to QTM and keeps the connection alive.

the6doflabels = QCM('6doflabels');
labels=deblank(string(the6doflabels)); % Convert to string array

% Mapping of labels (index to rigid body)
i_object=ismember(labels,object);

% Initialize figure
% - Figure parameters
nax=6;
nframes=500;
x_margin=50;
y_factor=1.1;
y_range_min=2;
%ylims=repmat([-1 1],nax,1); % Initial axis limits without assumptions.
ylims=[-220 510; -160 340; 80 700; -60 80; -70 40; -110 70]; % Fixed axis limits for the file 6d_and_analog.qtm
ylabs={'X','Y','Z','Yaw','Pitch','Roll'};

% - Plot data buffer
ydata=nan(nframes,nax);

% - Set up figure (whole screen)
figure('units', 'normalized', 'outerposition', [0 0 1 1]);

axis([-100   600    60   160   160   340]);
grid on
hold on

% State variables
skipped_frames=0;
last_frame=nan;

% Real time loop
for i_frame = 1:n
    % ### Fetch data from QTM
    [frameinfo,frame_6d] = QCM;
    
    current_frame=frameinfo(1);
    shift=current_frame-last_frame; % Calculate shift (1 frame, if no skipped frames)
    
    % Update data buffer
    ydata(1:nframes-shift,:)=ydata(1+shift:nframes,:); % Shift data to the left
    ydata(nframes,:)=frame_6d(i_object,1:nax); % Add current value
    if shift>1
        ydata(nframes-shift+1:nframes-1,:)=nan; % Set skipped frames to NaN (if any).
    end
    
    hh=[];
    if mod(i_frame,plot_update_interval)==0 % Update plot data and redraw at chosen plot rate interval
        if ~isempty(hh)
            delete(hh)
        end
        hh=scatter3(ydata(:,1),ydata(:,2),ydata(:,3),'filled','r');
        view(3)
        
        % Update figure (use limitrate and nocallbacks for best real time performance)
        drawnow limitrate nocallbacks
    end
    
    % Update state (count missed frames, loop time exceeds frame rate)
    % Tip: use multithread processing when you have the Matlab parallel
    % computing toolbox. One thread for updating buffer (RT communication)
    % and another for processing and plotting.
    skipped_frames=skipped_frames+shift-1; % Count skipped frames
    last_frame=current_frame;
end

% Report skipped frames
fprintf('Skipped frames: %u/%d\n',skipped_frames,n);

% Terminate QMC object and clear
QCM('disconnect');
clear mex
